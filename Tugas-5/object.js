//Soal 1
function arrayToObject(arr) {
    for (var i = 0;  i < arr.length; i++) {
 
        var thisYear = (new Date()).getFullYear();
        
        var personarr = arr[i]

         var objperson = {
            firstname : personarr[0],
            lastname  : personarr[1],
            gender  : personarr[2],
            age  : personarr[3],
         }
    if (!personarr[3] || personarr [3] > thisYear){
        objperson.age = "ivalid Birth Year"
    }else{
        objperson.age = thisYear - personarr[3]
    }
        var fullname = objperson.firstname + " " + objperson.lastname
        console.log(`${i + 1} . ${fullname} : `, objperson);
    }
}
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
arrayToObject([])

// Soal 2

function naikAngkot(arrPenumpang) {
var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
var output = []
for (var a = 0; a < arrPenumpang.length; a++) {
     var penumpangs = arrPenumpang[a]
     var obj = {
        penumpang : penumpangs[0],
        naik : penumpangs[1],
        tujuan : penumpangs[2]
     }
    
     var bayar = (rute.indexOf(penumpangs[2]) - rute.indexOf(penumpangs[1])) *2000

     obj.bayar = bayar

     output.push(obj)
}
return output
}
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]))
console.log(naikAngkot([]));

//soal 3

function nilaiTertinggi(siswa) {
    var output = {}
    for (var b = 0; b < siswa.length; b++){
    var current = siswa[b]
    if (!output[current.class]) {
       output[current.class] = {
        name: current.name,
        score: current.score
       }
    } else {
        if (current.score > output[current.class].score){
          output[current.class] ={
            name: current.name,
            score: current.score
          }
        }
    }
    }
    return output
 }
  
  // TEST CASE
  console.log(nilaiTertinggi([
    {
      name: 'Asep',
      score: 90,
      class: 'adonis'
    },
    {
      name: 'Ahmad',
      score: 85,
      class: 'vuejs'
    },
    {
      name: 'Regi',
      score: 74,
      class: 'adonis'
    },
    {
      name: 'Afrida',
      score: 78,
      class: 'reactjs'
    }
  ]));
  console.log(nilaiTertinggi([
    {
      name: 'Bondra',
      score: 100,
      class: 'adonis'
    },
    {
      name: 'Putri',
      score: 76,
      class: 'laravel'
    },
    {
      name: 'Iqbal',
      score: 92,
      class: 'adonis'
    },
    {
      name: 'Tyar',
      score: 71,
      class: 'laravel'
    },
    {
      name: 'Hilmy',
      score: 80,
      class: 'vuejs'
    }
  ]));